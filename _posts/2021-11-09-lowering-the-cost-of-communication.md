---
layout:     post
title:      "Lowering the Cost of Communication"
subtitle:   "Building document system for efficient teamwork"
date:       2021-11-09 12:00:00
author:     "Hecmay"
catalog: false
header-style: text
tags:
  - productivity
  - investigation
---

TL;DR. this blog post has two parts: (1) the important question: what specific features do I need from a document system? (2) which document system to use? and a comparison

## Criteria for document systems
- Cost (i.e., what's the price?)
- UX (e.g., Markup, WYSIWYG, UI design)
- Collaborations (e.g., comments, collaborative writings)
- Portability (e.g., editable, viewable on all devices)
- Extensibility (e.g., install custom plugins)
- Database (coupled or decoupled, i.e., head-less CMS)

## Comparison between doc systems
- Notion, Trillium, OneNote: 

- Gitbook, HackMD:

- JAMStack + VS Code: 

- Dokuwiki, Gitit

## Conclusion 
- Dokuwiki (or other free wikis like gitit) is the best solution for doc sharing within a team. 

- There are many alternatives to Docuwiki with much better UX and portability (e.g., gitbook or notion), but they are crazy expensive for a large team with more than 5 members.